sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast"
], function (Controller,JSONModel,MessageToast) {
	"use strict";

	return Controller.extend("Tutoriales.JSONModel.controller.View1", {
		onInit: function () {
			// set data model on view
			var oData = {
				recipient: {
					name: "World"
				}
			};
			var oModel = new JSONModel(oData);
			this.getView().setModel(oModel,"data");
		},
		onShowHello : function () {
         MessageToast.show("Hello World");
      }
	});
});